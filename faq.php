<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAQ LMC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/22fdf35712.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="scripts/scripts.js"></script>
  </head>

<body>
      <section class="hero is-fullheight is-dark hero-faq-presentation">
        <div class="hero-head">
            <?php require 'menu.php'; ?>
        </div>
        <div class="hero-body">
          <div class="container">
              <div class="columns is-justify-content-start">
                <div class="column is-three-fifths">
                  <h1 class="is-size-big is-size-1-mobile has-text-fuchsia has-text-weight-bold"> FAQ </h1>
                  <h1 class="is-size-big is-size-1-mobile has-text-weight-bold">Trouvez les réponses à vos questions</h1>
                  <p class="subtitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda 
                    consequatur impedit nobis similique adipisci quibusdam, 
                    nam enim doloribus dolore, quo quas ducimus nesciunt earum, dolorem qui eum reprehenderit. Nostrum, ipsam.</p>
                </div>
              </div>
            </div>
        </div>
        <div class="hero-footer">

        </div>
      </section>

      <section class="hero is-fullheight">
        <div class="hero-body">
          <div class="container">
            <div class="columns is-justify-content-center">
              <div class="column py-50">
                <h1 class="title"> Celles-ci sont les questions posées le plus fréquent</h1>
                <p class="subtitle"> Si vous ne trouvez pas la réponse à votre question, il ne faudrait que <a href="contact.php" class="has-text-fuchsia"> nous contacter !  </a> </p>
              </div>
            </div>
            <div class="columns">
              <div class="column px-0 is-flex is-flex-direction-column is-align-items-center is-full has-background-dark faq-questions">
                
                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-1" class="px-5"> 1. C'est quand le bon moment pour faire un placement?  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-1" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-2" class="px-5"> 2. question 2  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-2" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-3" class="px-5"> 3. question 3  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-3" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-4" class="px-5"> 4. question 4  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                 
                </div>
                <div id="response-4" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-5" class="px-5"> 5. question 5  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-5" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-6" class="px-5"> 6. question 6  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-6" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-7" class="px-5"> 7. question 7  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-7" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-8" class="px-5"> 8. question 8  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                 
                </div>
                <div id="response-8" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold">
                  <p id="question-9" class="px-5"> 9. question 9  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                
                </div>
                <div id="response-9" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

                <div class="question has-text-white has-text-weight-bold no-border">
                  <p id="question-10" class="px-5"> 10. question 10  
                    <span class="icon-text">
                      <span class="icon has-text-fuchsia">
                        <i class="fas fa-angle-down"></i>
                      </span>
                    </span>       
                  </p>                 
                </div>
                <div id="response-10" class="response has-background-silver is-hidden">
                  <p class="px-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsa ducimus nisi sint beatae in perspiciatis rerum quibusdam,
                     saepe nemo veritatis ut esse, maxime fugit harum mollitia ab quam aliquam repudiandae!</p>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="hero is-dark is-medium is-bold">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-full">
                        <h1 class="title has-text-centered has-text-white"> Avez-vous toujours de questions ? </h1>
                        <p class="subtitle has-text-centered has-text-white">Excellent ! contactez-nous et il nous fera plaisir de vous répondre</p>
                    </div>
                </div>
                <div class="columns is-justify-content-center">
                    <div class="column is-flex is-justify-content-center is-narrow">
                        <a href="contact.php" class="button is-fuchsia-outlined is-large has-text-white"> Contactez-nous </a>
                    </div>
                </div>
            </div>
        </div>
      </section>

    <?php require 'footer.php'; ?>
    
</body>
</html>