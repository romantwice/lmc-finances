let partenairesList = [
    `<div class="columns is-full">
      <div id="column-partenaires-1" class="column">
          <figure class="image is-250x250">
              <img src="images/logo_SFL_Placements.png" alt="SFL logo">
          </figure>
      </div>
      <div id="column-partenaires-2" class="column">
          <figure class="image is-250x250">
              <img src="images/flower.png" alt="SFL logo">
          </figure>
      </div>
      <div id="column-partenaires-3" class="column">
          <figure class="image is-250x250">
              <img src="images/logo_SFL_Placements.png" alt="SFL logo">
          </figure>
      </div>
    </div>`
    ,

    `<div class="columns is-full">
      <div id="column-" class="column">
          <figure class="image is-250x250">
          <img src="images/logo_SFL_Placements.png" alt="SFL logo">
          </figure>
      </div>
      <div id="column-part-2" class="column">
          <figure class="image is-250x250">
              <img src="images/logo_SFL_Placements.png" alt="SFL logo">
          </figure>
      </div>
      <div id="colnaires-3" class="column">
          <figure class="image is-250x250">
              <img src="images/logo_SFL_Placements.png" alt="SFL logo">
          </figure>
      </div>
    </div>`
  ];

  const columnsLogoPartenaires1 = document.querySelector('#columns-logo-partenaires-1');
  const columnsLogoPartenaires2 = document.querySelector('#columns-logo-partenaires-2');
  const columnPartenaires2 = document.querySelector('#column-partenaires-2');
  const columnPartenaires3 = document.querySelector('#column-partenaires-3');
  const leftSliderButton = document.querySelector('#left-slider-button');
  const rightSliderButton = document.querySelector('#right-slider-button');
  if (leftSliderButton && rightSliderButton) {
    leftSliderButton.addEventListener("click", () => {
      
      const ArrayFirstValue = partenairesList[0];

      for (let i = 0; i < partenairesList.length; i += 1) {
        if (i === partenairesList.length - 1) {
          partenairesList[i] = ArrayFirstValue;
        } else {
          partenairesList[i] = partenairesList[i + 1];
        }
      }

      columnsLogoPartenaires1.animate([
        {transform: 'translateX(-3000px)'}
      ], {
        duration: 500,
        fill: "both"
      });

      columnsLogoPartenaires2.animate([
        {transform: 'translateX(-3000px)'}
      ], {
        duration: 500,
        fill: "both"
      });

    });
  }
