<footer class="footer p-5 has-background-fuchsia-gradient">
        <div class="container">
            <div class="columns is-align-items-center">
                <div class="column is-one-third is-flex is-justify-content-center">
                    <figure class="image is-250x250">
                        <img src="images/logo_SFL_Placements.png" alt="SFL logo">
                    </figure>
                </div>
                <div class="column is-one-third is-flex is-justify-content-center">
                    <p class="has-text-light has-text-centered">
                        Les services financiers LMC Inc <i class="far fa-copyright" aria-hidden="true"></i> 2021 
                        <br> Website développé par <a href="https://romantwice.com" class="has-text-black"> Romantwice </a>
                    </p>
                </div>
                <div class="column is-one-third">
                    <div class="columns columns-social-media is-justify-content-space-around">
                        <div class="column is-flex is-justify-content-center is-narrow">
                            <a href="#">
                                <span class="icon has-text-white">
                                    <i class="fab fa-3x fa-facebook"></i>
                                </span>
                            </a>
                        </div>
                        <div class="column is-flex is-justify-content-center is-narrow">
                            <a href="#">
                                <span class="icon has-text-white">
                                    <i class="fab fa-3x fa-instagram"></i>
                                </span>
                            </a>
                        </div>
                        <div class="column is-flex is-justify-content-center is-narrow">
                            <a href="#">
                                <span class="icon has-text-white">
                                    <i class="fab fa-3x fa-linkedin"></i>
                                </span>
                            </a>
                        </div>
                        <div class="column is-flex is-justify-content-center is-narrow">
                            <a href="#">
                                <span class="icon has-text-white">
                                    <i class="far fa-3x fa-envelope"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>