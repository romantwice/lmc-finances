<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Services LMC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://kit.fontawesome.com/22fdf35712.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="scripts/scripts.js"></script>
  </head>

<body>
    <section class="hero is-fullheight is-dark hero-services-presentation">
        <div class="hero-head">
            <?php require 'menu.php'; ?>
        </div>
        <div class="hero-body">
          <div class="container">
              <div class="columns is-justify-content-start">
                <div class="column is-three-fifths">
                  <h1 class="is-size-big is-size-1-mobile has-text-fuchsia has-text-weight-bold"> Les services </h1>
                  <h1 class="is-size-big is-size-1-mobile has-text-weight-bold">que nous offreçons</h1>
                  <p class="subtitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Assumenda 
                    consequatur impedit nobis similique adipisci quibusdam, 
                    nam enim doloribus dolore, quo quas ducimus nesciunt earum, dolorem qui eum reprehenderit. Nostrum, ipsam.</p>
                </div>
              </div>
            </div>
        </div>
        <div class="hero-footer">

        </div>
    </section>

    <?php require 'footer.php'; ?>
    
</body>
</html>